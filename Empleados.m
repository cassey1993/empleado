//
//  Empleados.m
//  Empleados
//
//  Created by centro docente de computos on 29/04/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "Empleados.h"

@interface Empleados()

@property(nonatomic, strong) NSString * databasePath;

@end

@implementation Empleados



-(void)searchPathDatabase{
    NSString * rutaDoc= [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    _databasePath = [[NSString alloc]initWithString:[rutaDoc stringByAppendingPathComponent:@"empleados.db"]];
}

-(void)createDatabaseInDocuments{
    
    [self searchPathDatabase];
    
    NSFileManager *fm= [[NSFileManager alloc] init];
    //buscar la ruta y guardarla
    NSLog(@"%@", _databasePath);
    
    //buscar el archivo, lo demas es c puro
    
    if ([fm fileExistsAtPath:_databasePath]==NO) {
        NSLog(@"El archivo no existe");
        //C puro se convierte en UTF8string como si fuera un array de caracteres
        const char * dbpath=[_databasePath UTF8String];
        //abrir mi base de datos, con parametros , ruta base de datos y la direccion de memoria de la variable sqlite3, y ocn esto retorna si el archivo no esta lo crea
        if (sqlite3_open(dbpath, &empleadosdb)==SQLITE_OK) {
            NSLog(@"El archivo fue creado");
            //va manejar el error
            char * errMsg;
            //my query, creando la tabla sino existe
            const char * sql_stmt= "CREATE TABLE IF NOT EXISTS EMPLOYESS(ID INTEGER PRIMARY KEY AUTOINCREMENT, EMP_CEDULA TEXT, EMP_NAME TEXT, EMP_AGE TEXT, EMP_ADRESS TEXT)";
            //envio mi query sqlite3, SQLITE_OK  es una constante
            if (sqlite3_exec(empleadosdb, sql_stmt, NULL, NULL, &errMsg)==  SQLITE_OK) {
                NSLog(@"Tabla Creada Exitosamente!!..");
            }else{
                NSLog(@"Error en crear Tabla!!..:%s", errMsg);
            }
        }else{
            NSLog(@"Error en crear la base de datos");
        }
    }else{
        NSLog(@"El archivo ya existe, no se remplazo");
    }
    
}
-(void)searchEmployedInDataBasebyId:(NSString*)cedula{
    [self searchPathDatabase];
    const char*db = [_databasePath UTF8String];
    //hacer el query
    sqlite3_stmt * query;
    if (sqlite3_open(db, &empleadosdb)==SQLITE_OK)
    {
        NSString * select = [NSString stringWithFormat:@"SELECT * FROM EMPLOYESS WHERE EMP_CEDULA= \"%@\"", cedula];
        
        //convertir
        const char * select_sql = [select UTF8String];
        //-1 lo que mas oueda retornar
        if (sqlite3_prepare_v2(empleadosdb, select_sql, -1, &query, NULL)==SQLITE_OK) {
            //recorre un solo empleado por ello se utiliza un if
            if (sqlite3_step(query)==SQLITE_ROW) {
                _status=@"Registro encontrado";
                //con @"%s" para pasar de c a objetive c
                _empId= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 0)];
                _empCedula=[NSString stringWithFormat:@"%s", sqlite3_column_text(query, 1)];
                _empName= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 2)];
                _empAge= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 3)];
                _empAdress= [NSString stringWithFormat:@"%s", sqlite3_column_text(query, 4)];
            }else{
                _status=@"Registro no encontrado";
            }
        }else{
            _status=@"Error en el query";
        }
        //finalizo el query y cierro la base de datos
        sqlite3_finalize(query);
        sqlite3_close(empleadosdb);
    }else{
        _status =@"No se puede abrir la base de datos";
    }
    
}
-(void)createEmployedInDataBase{
    [self searchPathDatabase];
    sqlite3_stmt * query;
    const char * db= [_databasePath UTF8String];
    //& direccion de memoria
    if (sqlite3_open(db, &empleadosdb)==SQLITE_OK) {
        NSString * insert = [NSString stringWithFormat:@"INSERT INTO EMPLOYESS (EMP_CEDULA, EMP_NAME, EMP_AGE, EMP_ADRESS) VALUES(\"%@\",\"%@\",\"%@\",\"%@\")",_empCedula,_empName, _empAge, _empAdress];
        
        const char * insert_sql = [insert UTF8String];
        sqlite3_prepare_v2(empleadosdb, insert_sql, -1, &query, NULL);
        if (sqlite3_step(query)==SQLITE_DONE) {
            _status = @"Registro Almacenado";
        }else{
            _status = @"Registro No Almacenado";
        }
        sqlite3_finalize(query);
        sqlite3_close(empleadosdb);
    }else{
        _status= @"No se pudo abrir la base de datos";
    }
}

-(void)deleteEmployedFromDatabase:(NSString*)cedula{
    [self searchPathDatabase];
    sqlite3_stmt * query;
    const char * db=[_databasePath UTF8String];
    if (sqlite3_open(db, &empleadosdb)==SQLITE_OK) {
        NSString * delete =[NSString stringWithFormat:@"DELETE FROM EMPLOYESS WHERE EMP_CEDULA = \"%@\"", cedula];
        const char * delete_sql =[delete UTF8String];
        sqlite3_prepare_v2(empleadosdb, delete_sql, -1, &query, NULL);
        if (sqlite3_step(query)==SQLITE_DONE) {
            _status =@"Registro eliminado!";
        }else{
            _status =@"Registro no existe";
            
        }
        sqlite3_finalize(query);
        sqlite3_close(empleadosdb);
    }else{
        _status=@"Fallo al acceder a la base de datos";
    }
}


-(void)updateEmployedInDatabase{
    
    [self searchPathDatabase];
    sqlite3_stmt *query;
    const char * db=[_databasePath UTF8String];
    if (sqlite3_open(db, &empleadosdb)==SQLITE_OK) {
        
        NSString * update =[NSString stringWithFormat:@"UPDATE EMPLOYEES SET EMP_NAME= \"%@\", EMP_AGE= \"%@\", EMP_ADRESS= \"%@\"  WHERE EMP_CEDULA = \"%@\"", _empName,_empAge,_empAdress];
        const char * update_sql =[update UTF8String];
        
        sqlite3_prepare_v2(empleadosdb, update_sql, -1, &query, NULL);
        if (sqlite3_step(query)==SQLITE_DONE) {
            _status = @"Registro Actualizado con exito";
            // char * errMsg;
            //sqlite3_exec(alumnosdb, "commit", NULL, NULL, &errMsg);
            
            
        }else{
            _status =@"Fallo al actualizar registro";
        }
        
        
        sqlite3_finalize(query);
        sqlite3_close(empleadosdb);
        
        
        
    }else{
        _status =@"Fallo al acceder a la base de datos";
    }
}






@end
